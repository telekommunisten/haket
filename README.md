# Haket


rational cryptomoney


The HAKET is a token whose value is directly proportional to the amount of work/labour required to produce it. I.e it has a stable work:coin ratio.  This means it distinguishes itself from Bitcoin in some vital respects which allow it to be considered a money, whereas Bitcoin cannot be considered money.  Most importantly, the difficulty and therefore the reward is dynamic, that is difficulty increases according to demand for the currency and the reward for miners also concurrently increases.  This is the rational aspect of the currency. In Bitcoin, there is a discrepancy between the amount of “work” which is required to mine a block and the reward which is fixed for a period and then jumps periodically.  The HAKET block reward is dynamic and always proportional (rational) to the difficulty.   Furthermore, the HAKET has no arbitrary eventual limit of issued coins.  As Bitcoin begins to reach the limit of 21 million bitcoins, the difficulty will increase exponentially and though the reward is adjusted arbitrarily at various junctures creating disparities which are seized on for primarily arbitrage-based speculation.


"whitepapaer in progress" pad is [here](https://pad.riseup.net/p/H@k3t-keep) https://pad.riseup.net/p/H@k3t-keep
